from django.db import models

# Create your models here.
class Autor(models.Model):
    author_name = models.CharField(max_length=50, blank=False)
    email = models.EmailField()

    def __unicode__():
        return self.name

class Blog(models.Model):
    author = models.ForeignKey(Autor)
    title = models.CharField(max_length=50, blank=False)
    body = models.TextField(max_length=200, blank=True)

    def __unicode__():
        return self.title