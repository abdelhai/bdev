from django.contrib import admin
from batata.models import Autor, Blog

# Register your models here.
admin.site.register(Autor)
admin.site.register(Blog)